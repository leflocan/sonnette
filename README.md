# Sonnette de TVn7

Version web de la sonnette

## Principe

Permet de prévenir les gens au local que des personnes se trouvent en bas de la TR.

La page / propose un bouton qui fait sonner la page /play .

**Techno: NodeJS, socket.io**

## Deployment

1. Build de l'image sonnette

```build
$ docker build -t sonnette .
```

2. Ajout de l'image dans k8s

```Ajout
$ minikube cache add sonnette
```

3. Mise en place sur k8s

```
$ kubectl apply -f .\sonnette-deployment.yaml
$ kubectl apply -f .\sonnette-service.yaml
$ kubectl rollout status deployment.v1.apps/sonnette
$ minikube service sonnette
$ kubectl port-forward service/sonnette 7081:3000
```

## TO DO

Améliorer le CSS pour mobile et le rendre responsive ^^

N'autoriser que les co dans 147.127.0.0/16. Faire dans express une redirection en fct de l'@IP. Renvoyer ma page "tocard" quand on a pas la bonne @. Quand on recharge la page, renvoyer sur / si on a la bonne @.

Ne pas play en local.

## Install du script python en local

```
$ sudo apt install mpg123
$ pip install "python-socketio[client]"
```

## Service (install)

```
$ sudo nano /etc/systemd/system/sonnette.service
$ sudo systemctl daemon-reload
$ sudo systemctl enable sonnette.service
$ sudo systemctl start sonnette.service
```
