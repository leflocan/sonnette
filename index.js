const express = require("express");
const app = express();
const http = require("http");
const server = http.createServer(app);
const { Server } = require("socket.io");
const io = new Server(server);
const RELATIVE_PATH = "/home/n7/Bureau/SocketIo/chat-example";
var requestIp = require("request-ip");
var ipRangeCheck = require("ip-range-check");

app.get("/play", (req, res) => {
  res.sendFile(__dirname + "/play.html");
});

app.get("/sonnette.mp3", (req, res) => {
  res.sendFile(__dirname + "/sonnette.mp3");
});

app.get("/", function (req, res) {
  var clientIp = requestIp.getClientIp(req);
  console.log(clientIp);

  if (
    ipRangeCheck(clientIp, "147.127.0.0/16") ||
    ipRangeCheck(clientIp, "192.168.0.0/16") ||
    ipRangeCheck(clientIp, "10.0.0.0/8") ||
    ipRangeCheck(clientIp, "172.16.0.0/12")
  ) {
    res.sendFile(__dirname + "/index.html");
  } else {
    res.sendFile(__dirname + "/tocard.html");
  }
  return res;
});

io.on("connection", (socket) => {
  console.log("a user connected");
  socket.on("disconnect", () => {
    console.log("user disconnected");
  });
});

io.on("connection", (socket) => {
  socket.on("chat message", (msg) => {
    console.log("chat message", msg);
    io.emit("chat message", msg);
  });
});

server.listen(3000, () => {
  console.log("listening on *:3000");
});
