FROM node:16.14.2
WORKDIR /sonette
COPY . .
RUN npm i
EXPOSE 3000
CMD ["node","/sonette/index.js"]